import sys
#alumos@urjc
def es_correo_electronico(string):
    for i in range(0, len(string)):
        if string[i] == "@":
            if string[i - 1] == str(string[i - 1]):
                if string[i + 1] == str(string[i + 1]):
                    for n in range(i, len(string)):
                        if string[n] == ".":
                            return True
def es_entero(string):
    try:
        if int(string) == int(string):
            return True
        else:
            return False
    except:
        es_real(string)
def es_real(string):
    try:
        if float(string) == float(string):
            return True
        else:
            return False
    except:
        return False
def evaluar_entrada(string):
        if string == "":
            print("debes meter un string correcto")
            return None
        if es_correo_electronico(string):
            return "Es un correo electrónico."
        elif es_entero(string):
            return "Es un entero."
        elif es_real(string):
            return "Es un número real."
        else:
            return "No es ni un correo, ni un entero, ni un número real."


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()